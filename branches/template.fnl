#!/bin/env fennel
(local template {})
(set template.name "template")


;;; code:


(fn template.install [args]
  "function to be overridden later"
  (print "this is a template install function")
  (print "if you see this, either you or the developer making this module forgot to override the install function")
  )

(fn template.configure [args]
  "function to be overridden later"
  (print "this is a template setup function")
  (print "if you see this, either you or the developer making this module forgot to override the setup function")
  )

(fn template.new [base-object clone-object]
  (when (not= (type base-object) :table)
    (local retval (or clone-object base-object))
    (lua "return retval")
    )
  (set-forcibly! clone-object (or clone-object {}))
  (set clone-object.__index base-object)
  (setmetatable clone-object clone-object))

template
