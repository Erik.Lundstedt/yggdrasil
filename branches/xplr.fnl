#!/bin/env fennel
(var xplr {})
(local template (require :branches.template))
(set xplr (template:new))


(local paru (require :lib.paru))
(local git (require :lib.git))
(local env (require :lib.env))

;;; code:
(set xplr.name "xplr")

(fn xplr.install []
  (paru.install "xplr-git")
  )
(fn xplr.configure []
  (git.clone (.. env.cfgDir "/xplr") "dummy git repo")
  )

xplr
