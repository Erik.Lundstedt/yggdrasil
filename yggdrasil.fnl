#!/bin/env fennel
(local yggdrasil {})

;;; code:
(local branches
       [
        :dummy
        :xplr
        ]
        )

(fn install [branches]
  (each [key branch (ipairs branches)]
    (local br (require (.. "branches." branch)))
    (local spacer "#################################################")
    (print (.. spacer br.name spacer))
    (br.install)
    (br.configure)
    )
  )





(install branches)

yggdrasil
